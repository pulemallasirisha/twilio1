//sending sms using twilio

const twilio = require('twilio');
require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID; 
const authToken = process.env.TWILIO_AUTH_TOKEN;  

const client = new twilio(accountSid, authToken);

client.messages.create({
    body: 'hii, friend!',
    to: '+919703913597', 
    from: '+12765826839' 
})
.then((message) => console.log(message.sid));


// sending voice messages using twilio

const twilio = require('twilio');
require('dotenv').config();

const accountSid1 = process.env.TWILIO_ACCOUNT_SID; 
const authToken1 = process.env.TWILIO_AUTH_TOKEN;  

const client1 = new twilio(accountSid1, authToken1);

client1.calls.create({
    url: 'http://demo.twilio.com/docs/voice.xml',
    to: '+919703913597', 
    from: '+12765826839' 
})
.then((message) => console.log(message.sid));


//sending vedio messages